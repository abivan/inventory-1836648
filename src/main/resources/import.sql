INSERT INTO product (reference, name ) values ('1', 'Portatil'), ('2', 'Teclado'), ('3', 'Mouse');
INSERT INTO rols(name) values('ROLE_USER'), ('ROLE_ADMIN');
INSERT INTO users(username, password, name, last_name, email, enabled) values('abivan', '$2a$10$jBTLtorkcgNtoNTRx3cULOADQFex93Zf2oQO0uGHKmhJ9yXTeEoNG', 'ivan', 'agudelo', 'abivan@misena.edu.co', true);
INSERT INTO users(username, password, name, last_name, email, enabled) values('ybravo', '$2a$10$C3Uln5uqnzx/GswADURJGOIdBqYrly9731fnwKDaUdBkt/M3qvtLq', 'yohon', 'bravo', 'ybravo@misena.edu.co', true);
INSERT INTO user_has_rol values (1,1), (1,2);
INSERT INTO user_has_rol values (2,1);
