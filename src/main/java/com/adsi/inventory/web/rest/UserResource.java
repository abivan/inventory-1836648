package com.adsi.inventory.web.rest;

import com.adsi.inventory.service.ProductService;
import com.adsi.inventory.service.UserService;
import com.adsi.inventory.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class UserResource {

    @Autowired
    UserService service;

    @PostMapping("/user")
    public ResponseEntity<?> create(@RequestBody UserDTO userDTO) {
        UserDTO dto = null;
        Map<String, Object> response = new HashMap<>();
        try {
            dto = service.save(userDTO);
        } catch (DataAccessException err) {
            response.put("mensaje", "Error al guardar el usuario");
            response.put("error", err.getMessage() + ": " +err.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        response.put("mensaje", "el usuario fue creado satisfactoriamente");
        response.put("usuario", dto);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/user")
    public ResponseEntity<List<UserDTO>> get() {
        return ResponseEntity.status(200).body(service.getAll());
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<UserDTO> getById(@PathVariable Long id) {
        return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
    }

    @GetMapping("/user/account")
    public ResponseEntity<UserDTO> getAccount(@RequestParam(value = "username") String username) {
        return new ResponseEntity<>(service.findByUsername(username), HttpStatus.OK);
    }

}
