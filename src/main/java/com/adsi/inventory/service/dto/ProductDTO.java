package com.adsi.inventory.service.dto;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Getter
@Setter
public class ProductDTO implements Serializable {

    @Id
    private String reference;

    @Column(length = 20)
    private String name;
}
