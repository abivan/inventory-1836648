package com.adsi.inventory.service.dto;

import com.adsi.inventory.domain.Rols;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class UserDTO implements Serializable {

    @Id
    private Long id;

    private String username;

    private String name;
    private String lastName;

    private String email;

    private String password;

    private Boolean enabled;

    private List<Rols> rols;

    private List<String> authorities;
}
