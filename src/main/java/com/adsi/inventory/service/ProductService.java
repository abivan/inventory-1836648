package com.adsi.inventory.service;

import com.adsi.inventory.service.dto.ProductDTO;

import java.util.List;

public interface ProductService {
    public List<ProductDTO> getAll();

    public ProductDTO save(ProductDTO productDTO);

    public ProductDTO getByReference(String reference);
}
