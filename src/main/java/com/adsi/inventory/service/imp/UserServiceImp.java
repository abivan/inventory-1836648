package com.adsi.inventory.service.imp;

import com.adsi.inventory.domain.Users;
import com.adsi.inventory.repository.UserRepository;
import com.adsi.inventory.service.UserService;
import com.adsi.inventory.service.dto.UserDTO;
import com.adsi.inventory.service.dto.UserTransformer;
import com.adsi.inventory.service.error.ObjectNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImp implements UserService {

    private Logger logger = LoggerFactory.getLogger(UserServiceImp.class);

    @Autowired
    UserRepository repository;

    @Override
    public List<UserDTO> getAll() {
        return repository.findAll()
                .stream()
                .map(UserTransformer::getUserDTOFromUsers)
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO save(UserDTO userDTO) {
        return UserTransformer.getUserDTOFromUsers(repository.save(
                UserTransformer.getUsersFromUserDTO(userDTO)
        ));
    }

    @Override
    public UserDTO getById(Long id) {
        Optional<Users> user = repository.findById(id);
        if (!user.isPresent()) {
            throw new ObjectNotFoundException("El usuario con id " + id + " no fue encontrado");
        }
        return UserTransformer.getUserDTOFromUsers(user.get());
    }

    @Override
    public UserDTO findByUsername(String username) throws UsernameNotFoundException {
        Users user = repository.findByUsername(username);
        if (user == null) {
            logger.error("Error, username " + username + " no encontrado.");
            throw new UsernameNotFoundException("Error, username " + username + " no encontrado.");
        }

        UserDTO userDTO =  UserTransformer.getUserDTOFromUsers(user);
        ArrayList roles = new ArrayList<String>();
       userDTO.getRols().forEach(item -> {
           roles.add(item.getName());
       });
        userDTO.setAuthorities(roles);
        return userDTO;
      /*  List<GrantedAuthority> authorities = user.getRols()
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
        return new User(user.getUsername(), user.getPassword(), user.getEnabled(), true, true, true, authorities);*/
    }
}
