package com.adsi.inventory.auth;

public class JwtConfig {

    public static final String RSA_PRIVADA = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEpQIBAAKCAQEA0kxektZO94gn1nH4dePCXXDqPODW6P66ADEM731u2skxYjAm\n" +
            "/6M18yyxra0/polBSgBYkY9MNKUgMWcvI9dGNUUYNDWhaGgV7B1lqAqJnw1h9FsS\n" +
            "g0o5BZUHxjUXWHeIIbe86arMz3UwLN77fIC6N61iXtxUCE1yiu7nbRItgdjkUbob\n" +
            "aatL9CX6xkKAhk0T2Gw7hZcRQT71KxR4J27TCEZG2H/1PtDFfa4wVFIR+uutC0P9\n" +
            "qGWwZtr7taE8CpvCXBg9YIycn8vRDXRd55xKIQHeX0vJiUw3mkYCO5uqTpFMt/jX\n" +
            "NXO+mz+P+TbljGY1Q0fcnH3cypqe70YmDGNpHQIDAQABAoIBAE7ki442i8Vc9OUE\n" +
            "OJuxaBc55wYTq0wZU5oCzMGDovS9CaO2QMsiZXjmyXlIR1qony+t1Yw13/bYA63E\n" +
            "8fBWMGaHlz7u57CNa7v6g0p5OYvLr3XGPLm4PJAViqcW/70WiqFrlplqlA83/Wy3\n" +
            "8kyQ1ktQ5liD+5ppw5DPc059xUiycTIf80gCwmyDXXPDHUokG7RCdpLcBrewsmB7\n" +
            "KZYlvhGcMXAL4Bf0Fu9c2boSHvUZhaHeale/Wvh6PaVv3YTfKH5l+uChMqg3KofI\n" +
            "W/CxJiSWVInsnoW21hGyXC/zUtJCVo9+cWLDKaLjm5Adysd3wHF8z7EbXa6imNjc\n" +
            "bkqi00ECgYEA8Fxrr4Xkq2WHyMWDqi/D27emL40Bzkm21QYxfPPBafzjtkEbuGzA\n" +
            "WZsSy56eUurcEtw7Yl4BfBQNVLcJt65KZi1Sb0aBW3DvXGfF4D2abcO8fyDHaQ90\n" +
            "AvSWWby4PoiY5CsLNnHFdrG5zZmmuiUbHIVkumeVLX/WgBoa/GvFdtUCgYEA3/s1\n" +
            "ar6qYlcJ4Yt0Nfk54M443cnd3IhKm7Tf4K2dKij+yDD2EpbrbV6UYojOKZG2JuIA\n" +
            "gOdfUuq6mKZHWilqMJ8o1bef+O1JF1eJl5SBMjUITUULRpD81r4L+drrI5Pv3Jaf\n" +
            "RpSHEESDoWO8HxqMV4QLJY9xxD3qDEM29VM4XSkCgYEA0h85VHwsZ1me1nk0nPac\n" +
            "tdtaQH94cKooVxnO8Xj2QH1/Y7iPTy5jCPJz/+VuavY95Y75qwibK6kr7j8FE7od\n" +
            "+Czuv7BXLWURGyrs5Rd1hTdz9X7mVIXZ2SuQtGoydxha8Dxg1PuosBa2Y3JLOrdt\n" +
            "eAV1BS/ATcJP9jTcGobsXPECgYEAqXFWoNFq/ohQDlO8b5SCh6Ia/RZZBBwLLS4x\n" +
            "a7yLuDhZPsNjdP69TfkOEljidioofdPdg6cFwymWTiMEuZ25Gt1dPAj1OZDxCXqM\n" +
            "uaJ79qqOBD6mYBURYbdWQgeueTTScbN0/SUh5FmgpvhXecRQxWg8MocJDuez5uD2\n" +
            "c7MoYbkCgYEAnRMcGYQdCkRpqUFTUfR4/rjOTUIqaOdBuNSXbnllTBnJeSwKByTw\n" +
            "XYikBdkAENFu3Iz56kF81LoP4SfCRepFZi1Beu6tFqhLscIoWSHGdFjMXwxnj2Pe\n" +
            "xEnAD7rpQSkxFcH97AbgPDqv7zSyo3ebqp4b34tSD9Fy2NDCSUqWadk=\n" +
            "-----END RSA PRIVATE KEY-----";

    public static final String RSA_PUBLICA = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0kxektZO94gn1nH4dePC\n" +
            "XXDqPODW6P66ADEM731u2skxYjAm/6M18yyxra0/polBSgBYkY9MNKUgMWcvI9dG\n" +
            "NUUYNDWhaGgV7B1lqAqJnw1h9FsSg0o5BZUHxjUXWHeIIbe86arMz3UwLN77fIC6\n" +
            "N61iXtxUCE1yiu7nbRItgdjkUbobaatL9CX6xkKAhk0T2Gw7hZcRQT71KxR4J27T\n" +
            "CEZG2H/1PtDFfa4wVFIR+uutC0P9qGWwZtr7taE8CpvCXBg9YIycn8vRDXRd55xK\n" +
            "IQHeX0vJiUw3mkYCO5uqTpFMt/jXNXO+mz+P+TbljGY1Q0fcnH3cypqe70YmDGNp\n" +
            "HQIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
